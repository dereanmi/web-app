var assert = require('assert');
const Calculator = require('../calculator')

describe('Test calculate', function() {
  describe('#indexOf()', function() {
    it('5+5 should return 10', function() {
      assert.equal(new Calculator().plus(5,5),10);
    });

    it('5-5 should return 0', function() {
        assert.equal(new Calculator().minus(5,5),0);
    });

    it('5*5 should return 25', function() {
        assert.equal(new Calculator().mutiply(5,5),25);
    });

    it('5/5 should return 1', function() {
        assert.equal(new Calculator().divide(5,5),1);
    });

    it('5/5 should return 1', function() {
        assert.equal(new Calculator().divide(5,5),1);
    });

  });
});
