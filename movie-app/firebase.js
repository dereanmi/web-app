import firebase from "firebase/app";
import "firebase/app";
import "firebase/database";
import "firebase/auth";

const config = {
  apiKey: "AIzaSyCWsDrlLTolgkXAch0h98CIO-r9l3Lsy4g",
  authDomain: "workshop-dv-fa2db.firebaseapp.com",
  databaseURL: "https://workshop-dv-fa2db.firebaseio.com",
  projectId: "workshop-dv-fa2db",
  storageBucket: "workshop-dv-fa2db.appspot.com",
  messagingSenderId: "774544192913"
};
firebase.initializeApp(config);

firebase.initializeApp(config);

const database = firebase.database();
const auth = firebase.auth();
const provider = new firebase.auth.FacebookAuthProvider();

export { database, auth, provider };
