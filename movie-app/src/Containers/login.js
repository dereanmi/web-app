import React, { Component } from "react";
import { Form, Button, Input, Icon, message } from "antd";
import { withRouter } from "react-router-dom";
import { auth, provider } from "./firebase";

const KEY_USER_DATA = "user_data";
class login extends Component {
  state = {
    //set emailกับpassword ในonEmailChange ให้เป็นglobal สามารถใช้ได้ทั้งหมด
    email: "",
    password: "",
    user: null
  };

  loginFacebook = () => {
    auth.signInWithPopup(provider).then(({ user }) => {
      this.setState({ user });
      localStorage.setItem(
        KEY_USER_DATA,
        JSON.stringify({
          isLoggedIn: true,
          email: user.email
        })
      );
      this.props.history.replace("/movies");
    });
  };

  logoutFacebook = () => {
    auth.signOut().then(() => {
      this.setState({ user: null });
    });
  };

  navigateToMainPage = () => {
    const { history } = this.props;
    history.push("/movies");
  };

  componentDidMount() {
    const jsonStr = localStorage.getItem(KEY_USER_DATA);
    const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn;
    if (isLoggedIn) {
      // this.navigateToMainPage();
    }
  }

  onEmailChange = event => {
    //รับค่าจากการinput
    const email = event.target.value;
    this.setState({ email });
  };

  onPasswordChange = event => {
    const password = event.target.value;
    this.setState({ password });
  };

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email));
  }

  validatePassword(password) {
    var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
    return re.test(String(password));
  }

  onSubmitFormLogin = e => {
    const { history } = this.props;
    e.preventDefault(); //เวลากดsubmitหน้าเว็บจะไม่รีเฟรช
    const isValidEmail = this.validateEmail(this.state.email);
    const isValidPassword = this.validatePassword(this.state.password);
    console.log();
    if (isValidEmail && isValidPassword) {
      localStorage.setItem(
        //เช็คว่าuser loginรึยัง,เมื่อปิดbrowser cookieไม่หายด้วยการเก็บไว้ในlacal storage,กดย้อนกลับจะไม่กลับไปหน้าlogin
        KEY_USER_DATA,
        JSON.stringify({
          isLoggedIn: true,
          email: this.state.email
        })
      );
      this.navigateToMainPage();
    } else {
      message.error("Email or password invalid", 3);
    }
  };

  render() {
    return (
      <div>
        <h2>Login</h2>
        <p>{this.state.email}</p>
        <Form onSubmit={this.onSubmitFormLogin}>
          <Form.Item>
            <Input
              prefix={<Icon type="user" />}
              placeholder="Enter your username"
              onChange={this.onEmailChange}
            />
          </Form.Item>

          <Form.Item>
            <Input
              prefix={<Icon type="lock" />}
              type="password"
              placeholder="Enter your password"
              onChange={this.onPasswordChange}
            />
          </Form.Item>

          <Form.Item>
            <Button htmlType="submit">Submit</Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

export default withRouter(login);
