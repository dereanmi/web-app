import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import ListMovie from '../Component/ListMovie';
import ListFavorite from '../Component/favorite/list';
import Profile from '../Component/profile';

function RouteMenu(props) {
    return (
        <Switch>
            <Route
                path="/movies"
                exact
                render={() => {
                    return (
                        <ListMovie
                            items={props.items}
                        />
                    );

                }}
            />
            <Route path="/favorite" exact component={ListFavorite} /> {/*ไม่ต้องมีrender เพราะไม่มีprops*/}
            <Route path="/profile" exact component={Profile} />
            <Redirect from="/" exact to="/" />
        </Switch>
    )
}
export default RouteMenu;