import React, { Component } from "react";
import { Spin, Modal, Button, Layout, Menu, message, Pagination } from "antd"; //modalในantdใช้แสดงdialog
import RouteMenu from "./RouteMenu";
import { connect } from "react-redux";
//function Main(){
//   return (
//           <h1>Main page</h1>
//  )
//}

const { Header, Content, Footer, Sider } = Layout;
const menus = ["movies", "favorite", "profile"]; //เป็นarray ที่มี3ตัว คือhome favorit profile

const mapStateToProps = state => {
  return {
    isShowDialog: state.isShowDialog,
    itemMovieClick: state.itemMovieDetail
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDismissDialog: () =>
      dispatch({
        type: "dismiss_dialog"
      }),
    onItemMovieClick: item =>
      dispatch({
        type: "click_item",
        payload: item
      })
  };
};

class Main extends Component {
  state = {
    //เมื่อค่าเปลี่ยนจะupdate view,ใช้ได้ทั้งหมด
    items: [],
    isShowModal: false,
    itemMovie: null,
    pathName: menus[0], //เป็นarray ที่มี3ตัว คือhome favorit profile
    favItems: []
  };

  onClickOk = () => {
    this.props.onDismissDialog();
  };

  onModalClickCancel = () => {
    this.props.onDismissDialog();
  };

  componentDidMount() {
    const jsonStr = localStorage.getItem("list-fav"); //เซฟarray fav เก็บไว้ เมื่อเวลารีเฟรชจะได้ไม่หาย
    if (jsonStr) {
      const items = jsonStr && JSON.parse(jsonStr);
      console.log(items);
      this.setState({ favItems: items });
    }

    const { pathname } = this.props.location; //ที่อยู่
    var pathName = menus[0];
    if (pathname != "/") {
      {
        /*ไม่เท่ากับหน้าlogin*/
      }
      pathName = pathname.replace("/", "");
      if (!menus.includes(pathName)) pathName = menus[0]; //includeใช้ตรวจสอบชื่อในarray และreturn เป็นture/false
    }
    this.setState({ pathName });
    //TODO:get list movies from API
    fetch(
      "http://api.giphy.com/v1/gifs/search?q=fail&api_key=kQMTY45oyABcpk6SBarCPcMk2Im9gBPM"
    )
      .then(response => response.json())
      .then(movies => this.setState({ items: movies.data }));
  }

  onMenuClick = e => {
    var path = "/";
    if (e.key != "home") {
      path = `/${e.key}`;
    }
    this.props.history.replace(path);
  };

  onClickFavorite = () => {
    //เมื่อuserคลิกหัวใจ
    const itemClick = this.props.itemMovieClick; //ดูว่ากดหัวใจหนังเรื่องไหน
    const items = this.state.favItems;

    console.log(items);
    const result = items.find(item => {
      return item.title === itemClick.title; //เช็คในarrayว่ามีหนังเรื่องนี้หรือยัง
    });

    if (result) {
      message.error("This giphy added"); //SHOW ERROR มีหนังแล้ว
    } else {
      items.push(itemClick);
      //save item to local storage
      localStorage.setItem("list-fav", JSON.stringify(items)); //save arrayลงในlocal storage โดยsetเป็นstring
      message.success("Your favorite giphy is saved");
      this.onModalClickCancel();
    }
  };

  render() {
    const item = this.props.itemMovieClick;
    console.log(item);
    return (
      <div className="Main">
        {this.state.items.length > 0 ? (
          <div style={{ height: "100vh" }}>
            {""}
            <Layout className="layout" style={{ background: "orange" }}>
              <Header
                style={{
                  padding: "0px", //ขอบ
                  position: "fixed", //ตำแหน่ง
                  zIndex: 1, //แกนมไม่มีก็ได้
                  width: "100%"
                }}
              >
                <Menu
                  theme="dark" //มีdark
                  mode="horizontal" //แนวตั้ง แนวนอน
                  defaultSelectedKeys={[this.state.pathName]} //จุดแสดงselect เช่นselect home ปุ่มhomeจะเป็นสีฟ้า
                  style={{ lineHeight: "64px" }}
                  onClick={e => {
                    this.onMenuClick(e); //functionกดเมนู
                  }}
                >
                  <Menu.Item key={menus[0]}>Home</Menu.Item>
                  <Menu.Item key={menus[1]}>Favorite</Menu.Item>
                  <Menu.Item key={menus[2]}>Profile</Menu.Item>
                </Menu>
              </Header>
              <Content
                style={{
                  padding: "16px",
                  marginTop: 64,
                  minHeight: "600px",
                  justifyContent: "center", //แนวตั้ง
                  alignItems: "center", //แนวนอน
                  display: "flex" //ตัดdivให้เท่ากับcontent
                }}
              >
                <RouteMenu items={this.state.items} />
              </Content>

              <Footer style={{ textAlign: "center", background: "white" }}>
                Movie Application Workshop @ CAMT
              </Footer>
            </Layout>
          </div>
        ) : (
          <Spin size="large" />
        )}
        {item != null ? (
          <Modal
            width="40%"
            style={{ maxHeight: "70%" }}
            title={item.title}
            visible={this.props.isShowDialog} //จะshowหรือไม่
            onCancel={this.onModalClickCancel} //คลิกCancleได้
            footer={[
              <Button
                key="submit"
                type="primary"
                icon="heart"
                size="large"
                shape="circle"
                onClick={this.onClickFavorite}
              />,
              <Button
                key="submit"
                type="primary"
                icon="shopping-cart"
                size="large"
                shape="circle"
                onClick={this.onClickBuyTicket}
              />
            ]}
          >
            {item.images != null ? (
              <img
                src={item.images.fixed_width.url}
                style={{ width: "100%" }}
              />
            ) : (
              <div />
            )}
            <br />
            <br />
            <p>{item.overview}</p>
          </Modal>
        ) : (
          <div />
        )}
      </div>
    );
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
