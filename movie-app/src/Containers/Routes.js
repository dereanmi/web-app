import React from 'react'
import { Route, Switch } from 'react-router-dom'
import LoginPage from './login'
import MainPage from './Main'

function Routes() {
    return ( //switch จะมองเป็นcomponent เมื่อกดที่favoriteจะเปลี่ยนหน้า เพื่อกันไม่ให้renderทั้งหมด
        <div> 
            <Switch> 
                <Route exact path="/" component={LoginPage} />
                <Route exact component={MainPage} />
            </Switch>
        </div>
    );
}
export default Routes