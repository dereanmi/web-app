export default (
    state = {
        itemMovieDetail: {},
        isShowDialog: false
    },
    action
) => {
    switch (action.type) { //บอกว่าต้องการทำอะไรอยู่
        case 'click_item':
            return {
                isShowDialog: true,
                itemMovieDetail: action.payload //ใช้คู่กับaction.type
            };
        case 'dismiss_dialog':
            return {
                isShowDialog: false,
                itemMovieDetail: {}
            };
        default:
            return state;
    }
};