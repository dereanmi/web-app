import React from "react";
import { Card } from "antd";
import TextTruncate from "react-text-truncate"; //แสดงผลให้ข้อมูลเป็น...read more
import { connect } from "react-redux"; //connectของในproviders

const { Meta } = Card;

const mapDispatchToProps = dispatch => {
  //function,เรียกของจากprovider ผ่านprops
  return {
    onItemMovieClick: item =>
      dispatch({
        //function
        type: "click_item",
        payload: item
      })
  };
};

function ItemMovie(props) {
  const item = props.item; //ดึง itemมาเก็บไว้ในตัวแปรitem
  return (
    <Card //เป็นการจัดหน้าของการแสดงitem movie ตามantd
      onClick={() => {
        //user click
        props.onItemMovieClick(item); //เมื่อcardถูกคลิกจะรันคำสั่งนี้
      }}
      hoverable //เวลาmouseไปชี้จะเป็นรูปมือ
      cover={<img src={item.images.fixed_width.url} />}
    >
      <Meta //เป็นการแสดงข้อมูลรายละเอียด
        title={item.title}
        description={
          <TextTruncate //เลือกแสดงข้แมูลแค่1บรรทัด ที่เหลือเป็น...
            line={1}
            truncateText="..."
            text={item.overview}
            textTruncateChild={<a href="#">Read more</a>}
          />
        }
      />
    </Card>
  );
}
export default connect(
  null,
  mapDispatchToProps
)(ItemMovie);
