import React from 'react'
import ItemMovie from './ItemMovie'
import { List } from 'antd'

function onItemMovieClick(item) {
    console.log(`click item : ${item.title}`)
}

function ListMovie(props) { //functionในการประกาศitem movie โดยใช้คำสั่งprops
    return (
        <List
            grid={{ gutter: 16, column: 4 }}
            dataSource={props.items}
            renderItem={item => (
                <List.Item>
                    <ItemMovie item={item} onItemMovieClick={props.onItemMovieClick} />
                </List.Item>
            )}
        />
    );
}
export default ListMovie